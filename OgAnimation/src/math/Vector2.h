#pragma once

template<typename T>
struct TVector2
{
	T x;
	T y;

	explicit inline TVector2() : x(T(0)), y(T(0)) { }
	explicit inline TVector2(T p_x, T p_y) : x(p_x), y(p_y) { }
	explicit inline TVector2(T* fv) : x(fv[0]), y(fv[1]) { }

};

typedef TVector2<float> Vector2;
typedef TVector2<int>	iVector2;
