#pragma once

#include <iostream>

struct Vector3
{
	/*-----------------------------------------------------------------------------
	Globals
	-----------------------------------------------------------------------------*/
	static const Vector3 zeroVector;
	static const Vector3 unitVector;
	static const Vector3 upVector;
	static const Vector3 downVector;
	static const Vector3 forwardVector;
	static const Vector3 backwardVector;
	static const Vector3 rightVector;
	static const Vector3 leftVector;

	/*-----------------------------------------------------------------------------
	Attributes and constructors
	-----------------------------------------------------------------------------*/

	/*The x component of the vector*/
	float x;
	/*The y component of the vector*/
	float y;
	/*The z component of the vector*/
	float z;

	/*Constructors*/
	explicit inline Vector3() : x(0.0f), y(0.0f), z(0.0f) { }
	explicit inline Vector3(float p_x, float p_y, float p_z) : x(p_x), y(p_y), z(p_z) { }
	explicit inline Vector3(float* fv) : x(fv[0]), y(fv[1]), z(fv[2]) { }

	/*-----------------------------------------------------------------------------
	Methods
	-----------------------------------------------------------------------------*/

	/**
	* Equalize this to the v vector
	*/
	Vector3& operator=(const Vector3& v);

	/**
	* Return the dot product between the first and the second vector
	* 
	* @param v First vector
	* @param w Second vector
	*/
	static float dotProduct(const Vector3& v, const Vector3& w);

	/**
	* Return the cross product between the first and the second vector
	*
	* @param v First vector
	* @param w Second vector
	*/
	static Vector3 crossProduct(const Vector3& v, const Vector3& w);

	/**
	 * Return the length of this vector.
	 */
	float length();

	/**
	 * Return the length of the v vector.
	 */
	static float length(const Vector3& v);

	/**
	 * Return the length of this vector without sqtr.
	 */
	float lengthSqrt();

	/**
	 *Return the length of the v vector without sqtr.
	 */
	static float lengthSqrt(const Vector3& v);

	/**
	 * Return the distance between this and another vector v.
	 *
	 * @param v The other vector
	 */
	float distanceTo(const Vector3& v);

	/**
	 * Return the distance between v vector and w vector.
	 *
	 * @param v The first vector
	 * @param w The second vector
	 */
	static float distanceTo(const Vector3& v, const Vector3& w);

	/**
	 * Normalize this vector
	 * 
	 * @return true if the vector was normalized correctly
	 */
	bool normalize();

	/**
	 * Normalize the v vector
	 * 
	 * @param The other vector
	 * @return The vector normalized
	 */
	static Vector3 normalized(const Vector3& v);

	/**
	 * The angle between the v vector and the w vector
	 *
	 * @param The first vector
	 * @param The second vector
	 */
	static float angle(const Vector3& v, const Vector3& w, bool degrees = true);

	/**
	* Return the projection of v vector onto the vector w
	*
	* @param The v vector
	* @param The w vector
	*/
	static Vector3 project(const Vector3& v, const Vector3& w);

	/**
	* Return the rejection of v vector onto the vector w
	*
	* @param The v vector
	* @param The w vector
	*/
	static Vector3 reject(const Vector3& v, const Vector3& w);

	/**
	* Return the reflection of v vector onto the vector w
	*
	* @param The v vector
	* @param The w vector
	*/
	static Vector3 reflect(const Vector3& v, const Vector3& w);

	/**
	 * Linear interpolation between v vector and the vector w
	 *
	 * @param The v vector
	 * @param The w vector
	 * @param Time normalized
	 */
	static Vector3 lerp(const Vector3& v, const Vector3& w, float t);

	/**
	 * Spherical interpolation between v vector and the vector w
	 *
	 * @param The v vector
	 * @param The w vector
	 * @param Time normalized
	 */
	static Vector3 slerp(const Vector3& v, const Vector3& w, float t);

	/**
	 * Normalized linear interpolation between v vector and the vector w
	 *
	 * @param The v vector
	 * @param The w vector
	 * @param Time normalized
	 */
	static Vector3 nlerp(const Vector3& v, const Vector3& w, float t);


};

/*-----------------------------------------------------------------------------
Operators

Operators are NOT member functions to avoid left/right-hand problems
-----------------------------------------------------------------------------*/

Vector3 operator+(const Vector3& v, const Vector3& w);
Vector3 operator-(const Vector3& v, const Vector3& w);
Vector3 operator*(const Vector3& v, float scale);
Vector3 operator*(const Vector3& v, const Vector3& w);
Vector3 operator-(const Vector3& v);
bool operator==(const Vector3& v, const Vector3& w);
bool operator!=(const Vector3& v, const Vector3& w);
std::ostream& operator<<(std::ostream& os, const Vector3& v);