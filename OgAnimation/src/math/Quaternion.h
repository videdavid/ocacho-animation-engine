#pragma once

#include "core/CoreMinimal.h"

/*Necessary fast forward declarations*/
struct Matrix4;

struct Quaternion
{
	/*-----------------------------------------------------------------------------
	Attributes and constructors

	Union to be able to access the data in different ways
	-----------------------------------------------------------------------------*/

	union
	{
		struct
		{
			float x;
			float y;
			float z;
			float w;
		};

		struct
		{
			Vector3 vector;
			float scalar;
		};

		float v[4];
	};

	/*Constructors*/
	explicit inline Quaternion() : x(0.0f), y(0.0f), z(0.0f), w(1.0f) { }
	explicit inline Quaternion(float p_x, float p_y, float p_z, float p_w) : x(p_x), y(p_y), z(p_z), w(p_w) { }

	/*-----------------------------------------------------------------------------
	Methods
	-----------------------------------------------------------------------------*/

	/**
	 * Equalize this to the q quaternion
	 */
	Quaternion& operator=(const Quaternion& q);

	/**
	 * Return the dot product between a and b quaternions
	 */
	static float dotProduct(const Quaternion& a, const Quaternion& b);

	/**
	 * Create a quaternion based on an axis (direciton) and a desired angle
	 * 
	 * @param angle Degrees we want to rotate
	 * @param axis Axis about which we want to rotate
	 */
	static Quaternion angleAxis(float angle, const Vector3& axis);

	/**
	 * Create a quaternion which rotates from (from) to (to)
	 *
	 * @param angle Degrees we want to rotate
	 * @param axis Axis about which we want to rotate
	 */
	static Quaternion fromTo(const Vector3& from, const Vector3& to);

	/**
	 * Return the length of this quaternion.
	 */
	float length();

	/**
	 * Return the length of the q quaternion
	 */
	static float length(const Quaternion& q);

	/**
	 * Return the length of this quaternion without sqtr.
	 */
	float lengthSqrt();

	/**
	 *Return the length of the q quaternion without sqtr.
	 */
	static float lengthSqrt(const Quaternion& q);

	/**
	 * Normalize this quaternion
	 *
	 * @return true if the quaternion was normalized correctly
	 */
	bool normalize();

	/**
	 * Normalize the q quaternion
	 *
	 * @param The q quaternion
	 * @return The quaternion normalized
	 */
	static Quaternion normalized(const Quaternion& q);

	/**
	 * Return the conjugate of the q quaternion
	 */
	static Quaternion conjugate(const Quaternion& q);

	/**
	 * Return the inverse of the q quaternion
	 */
	static Quaternion inverse(const Quaternion& q);

	/**
	 * Return the axis of this quaternion
	 */
	Vector3 getAxis();

	/**
	 * Return the axis of the q quaternion
	 */
	static Vector3 getAxis(const Quaternion& q);

	/**
	 * Return the angle of this quaternion
	 * 
	 * @param degrees Whether we want it in degrees or radians
	 */
	float getAngle(bool degrees = true);

	/**
	 * Return the angle of the q quaternion
	 *
	 * @param degrees Whether we want it in degrees or radians
	 */
	static float getAngle(const Quaternion& q, bool degrees = true);

	/**
	 * Linear interpolation between from and to given a t
	 *
	 * @param t: The time from 0 to 1 -> 0 = a, 1 = b
	 * @return The interpolated quaternion
	 */
	static Quaternion mix(const Quaternion& from, const Quaternion& to, float t);

	/**
	 * Normalized linear interpolation between from and to given a t
	 *
	 * @param t: The time from 0 to 1 -> 0 = a, 1 = b
	 * @return The interpolated quaternion
	 */
	static Quaternion nlerp(const Quaternion& from, const Quaternion& to, float t);

	/**
	 * Spherical interpolation between from and to given a t
	 *
	 * @param t: The time from 0 to 1 -> 0 = a, 1 = b
	 * @return The interpolated quaternion
	 */
	static Quaternion slerp(const Quaternion& from, const Quaternion& to, float t);

	/**
	 * Create a quaternion which rotates in the desired direction (direction)
	 *
	 * @param direction Desired direction to rotate
	 * @param up Desired vector up direction
	 */
	static Quaternion lookRotation(const Vector3& direction, const Vector3& up);

	/**
	 * Return the Matrix4 of the q Quaternion
	 */
	static Matrix4 quaternionToMatrix4(const Quaternion& q);

	/**
	 * Return the Quaternion of the m Matrix4
	 */
	static Quaternion matrix4ToQuaternion(const Matrix4& m);

private:

	/**
	 * Return the sortest arc created from (from) to (to). Used to avoid the neihborhood problem
	 */
	static Quaternion neighborhood(const Quaternion& from, const Quaternion& to);
};

/*-----------------------------------------------------------------------------
Operators

Operators are NOT member functions to avoid left/right-hand problems
-----------------------------------------------------------------------------*/

Quaternion operator+(const Quaternion& q, const Quaternion& p);
Quaternion operator-(const Quaternion& q, const Quaternion& p);
Quaternion operator-(const Quaternion& q);
Quaternion operator*(const Quaternion& q, float v);
Quaternion operator*(const Quaternion& q, const Quaternion& p);
Vector3 operator*(const Quaternion& q, const Vector3& v);
Quaternion operator^(const Quaternion& q, float f);
bool operator==(const Quaternion& q, const Quaternion& p);
bool operator!=(const Quaternion& q, const Quaternion& p);
std::ostream& operator<<(std::ostream& os, const Quaternion& q);





