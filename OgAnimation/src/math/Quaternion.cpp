#include "Quaternion.h"
#include "core/CoreMinimal.h"
#include <cmath>

/*-----------------------------------------------------------------------------
Operators
-----------------------------------------------------------------------------*/

Quaternion operator+(const Quaternion& q, const Quaternion& p)
{
	return Quaternion(q.x + p.x, q.y + p.y, q.z + p.z, q.w + p.w);
}

//---------------------------------------------------
//-----------------

Quaternion operator-(const Quaternion& q, const Quaternion& p)
{
	return Quaternion(q.x - p.x, q.y - p.y, q.z - p.z, q.w - p.w);
}

//---------------------------------------------------
//-----------------

Quaternion operator-(const Quaternion& q)
{
	return Quaternion(-q.x, -q.y, -q.z, -q.w);
}

//---------------------------------------------------
//-----------------

Quaternion operator*(const Quaternion& q, float v)
{
	return Quaternion(q.x * v, q.y * v, q.z * v, q.w * v);
}

//---------------------------------------------------
//-----------------

Quaternion operator*(const Quaternion& q, const Quaternion& p)
{
	return Quaternion(
		p.x * q.w + p.y * q.z - p.z * q.y + p.w * q.x,
		-p.x * q.z + p.y * q.w + p.z * q.x + p.w * q.y,
		p.x * q.y - p.y * q.x + p.z * q.w + p.w * q.z,
		-p.x * q.x - p.y * q.y - p.z * q.z + p.w * q.w
	);
}

//---------------------------------------------------
//-----------------

Vector3 operator*(const Quaternion& q, const Vector3& v)
{
	//TODO: probar si con lo que esta en el libro en la pagina 88 es equivalente a ver que tal, aunque imagino que esto sera mas optimo, pero me renta mas hacerlo de la otra forma para entednerlo

	return q.vector * 2.0f * Vector3::dotProduct(q.vector, v) +
		v * (q.scalar * q.scalar - Vector3::dotProduct(q.vector, q.vector)) +
		Vector3::crossProduct(q.vector, v) * 2.0f * q.scalar;
}

//---------------------------------------------------
//-----------------

Quaternion operator^(const Quaternion& q, float f)
{
	float angle = Quaternion::getAngle(q,false);
	Vector3 axis = Quaternion::getAxis(q);

	float halfCos = cosf(f * angle * 0.5f);
	float halfSin = sinf(f * angle * 0.5f);

	return Quaternion(
		axis.x * halfSin,
		axis.y * halfSin,
		axis.z * halfSin,
		halfCos);
}

//---------------------------------------------------
//-----------------

std::ostream& operator<<(std::ostream& os, const Quaternion& q)
{
	os << "X:" << q.x << " |" << "Y:" << q.y << " |" << "Z:" << q.z << " |" << "W:" << q.w << "\n";
	return os;
}

//---------------------------------------------------
//-----------------

bool operator==(const Quaternion& q , const Quaternion& p)
{
	return fabs(Quaternion::dotProduct(q, p) - 1.0f) < EPSILON;
}

//---------------------------------------------------
//-----------------

bool operator!=(const Quaternion& q, const Quaternion& p)
{
	return !(q == p);
}

//---------------------------------------------------
//-----------------

Quaternion& Quaternion::operator=(const Quaternion& q)
{
	this->vector = q.vector;
	this->scalar = q.scalar;

	return *this;
}

/*-----------------------------------------------------------------------------
Methods
-----------------------------------------------------------------------------*/

float Quaternion::dotProduct(const Quaternion& a, const Quaternion& b)
{
	return a.x * b.x + a.y * b.y + a.z * b.z + a.w * b.w;
}

//---------------------------------------------------
//-----------------

Quaternion Quaternion::angleAxis(float angle, const Vector3& axis)
{
	Vector3 axisNormalized = Vector3::normalized(axis);
	float scalar = sinf(angle * 0.5f);

	return Quaternion(
		axisNormalized.x * scalar,
		axisNormalized.y * scalar,
		axisNormalized.z * scalar,
		cosf(angle * 0.5f)
	);
}

//---------------------------------------------------
//-----------------

Quaternion Quaternion::fromTo(const Vector3& from, const Vector3& to)
{
	//En el episodio ese que ponemos a girar como un mapa o no se si es la camara podemos ver para como meterselo a un actor
	//Revisar esto y enterarse bien de los quaternions pq vaya tela
	Vector3 n_from = Vector3::normalized(from);
	Vector3 n_to = Vector3::normalized(to);

	if (n_from == n_to)
	{
		std::cout << "The position of from and to is the same" << std::endl;
		return Quaternion();
	}
	else if(n_from == (n_to * -1.0f))
	{
		Vector3 ortho = Vector3(1, 0, 0);

		if(fabs(n_from.y) < fabs(n_from.x))
		{
			ortho = Vector3(0, 1, 0);
		}
		if(fabs(n_from.z) < fabs(n_from.y) && fabs(n_from.z) < fabs(n_from.x))
		{
			ortho = Vector3(0, 0, 1);
		}

		Vector3 axis = Vector3::normalized(Vector3::crossProduct(n_from, ortho));
		
		return Quaternion(axis.x, axis.y, axis.z, 0);
	}

	Vector3 half = Vector3::normalized(n_from + n_to);
	Vector3 axis = Vector3::crossProduct(n_from, half);

	return Quaternion(axis.x, axis.y, axis.z, Vector3::dotProduct(n_from, half));

}

//---------------------------------------------------
//-----------------

float Quaternion::length()
{
	if (this->lengthSqrt() < EPSILON) { return 0.0f; }

	return sqrtf(this->lengthSqrt());
}

//---------------------------------------------------
//-----------------

float Quaternion::length(const Quaternion& q)
{
	if (lengthSqrt(q) < EPSILON) { return 0.0f; }

	return sqrtf(lengthSqrt(q));
}

//---------------------------------------------------
//-----------------

float Quaternion::lengthSqrt()
{
	return dotProduct(*this, *this);
}

//---------------------------------------------------
//-----------------

float Quaternion::lengthSqrt(const Quaternion& q)
{
	return dotProduct(q, q);
}

//---------------------------------------------------
//-----------------

bool Quaternion::normalize()
{
	if (lengthSqrt() < EPSILON) { return false; }

	float inverLenght = 1.0f / sqrtf(lengthSqrt());

	x *= inverLenght;
	y *= inverLenght;
	z *= inverLenght;
	w *= inverLenght;

	return true;
}

//---------------------------------------------------
//-----------------

Quaternion Quaternion::normalized(const Quaternion& q)
{
	if (lengthSqrt(q) < EPSILON) { return Quaternion(); }

	float inverLenght = 1.0f / sqrtf(lengthSqrt(q));

	return Quaternion(q.x * inverLenght, q.y * inverLenght, q.z * inverLenght, q.w * inverLenght);
}

//---------------------------------------------------
//-----------------

Quaternion Quaternion::conjugate(const Quaternion& q)
{
	return Quaternion(-q.x, -q.y, -q.z, q.w);
}

//---------------------------------------------------
//-----------------

Quaternion Quaternion::inverse(const Quaternion& q)
{
	return conjugate(normalized(q));
}

//---------------------------------------------------
//-----------------

Vector3 Quaternion::getAxis()
{
	return Vector3::normalized(Vector3(x, y, z));
}

//---------------------------------------------------
//-----------------

Vector3 Quaternion::getAxis(const Quaternion& q)
{
	return Vector3::normalized(Vector3(q.x, q.y, q.z));
}

//---------------------------------------------------
//-----------------

float Quaternion::getAngle(bool degrees /*= true*/)
{
	if (degrees)
		return (2.0f * acosf(w) * TO_DEGREES);

	return 2.0f * acosf(w);
}

//---------------------------------------------------
//-----------------

float Quaternion::getAngle(const Quaternion& q, bool degrees /*= true*/)
{
	if (degrees)
		return (2.0f * acosf(q.w) * TO_DEGREES);

	return 2.0f * acosf(q.w);
}

//---------------------------------------------------
//-----------------

Quaternion Quaternion::neighborhood(const Quaternion& from, const Quaternion& to)
{
	if(dotProduct(from,to) < 0.0f)
	{
		return Quaternion(-to.x, -to.y, -to.z, -to.w);
	}

	return to;
}

//---------------------------------------------------
//-----------------

Quaternion Quaternion::mix(const Quaternion& from, const Quaternion& to, float t)
{
	return from * (1.0f - t) + neighborhood(from,to) * t;
}

//---------------------------------------------------
//-----------------

Quaternion Quaternion::nlerp(const Quaternion& from, const Quaternion& to, float t)
{
	return normalized(from + (neighborhood(from, to) - from) * t);
}

//---------------------------------------------------
//-----------------

Quaternion Quaternion::slerp(const Quaternion& from, const Quaternion& to, float t)
{
	if (fabs(dotProduct(from, to)) > 1.0f - EPSILON) { return nlerp(from, to, t); }

	/*Interpolation path*/
	Quaternion delta = inverse(from) * to;

	return normalized(delta ^ t) * from;
}

//---------------------------------------------------
//-----------------

Quaternion Quaternion::lookRotation(const Vector3& direction, const Vector3& up)
{
	//Esta funcion lo que hace es crear un quaternion en funcion de una direccion seleccionada, pero sin que influya la direccion up, por eso queremos mediante transformaciones y cosas anular ese movimiento en up pag 93 libro

	Vector3 f = Vector3::normalized(direction); //Object forward
	Vector3 u = Vector3::normalized(up);		//Desired up
	Vector3 r = Vector3::crossProduct(u, f);	//Object right
	u = Vector3::crossProduct(f, r);			//Desired object up

	//From world forward to object forward
	Quaternion worldToObject = fromTo(Vector3::forwardVector, f);

	//What direction is the new object up
	Vector3 objectUp = worldToObject * Vector3::upVector;

	//From object up to desired up
	Quaternion u2u = fromTo(objectUp, u);

	//Rotate to forward direction first
	//then twist to correct up

	//Don't forget to normalize the result
	return normalized(worldToObject * u2u);

}

//---------------------------------------------------
//-----------------

Matrix4 Quaternion::quaternionToMatrix4(const Quaternion& q)
{
	Vector3 r = q * Vector3::rightVector;
	Vector3 u = q * Vector3::upVector;
	Vector3 f = q * Vector3::forwardVector;

	return Matrix4(
		r.x, r.y, r.z, 0,
		u.x, u.y, u.z, 0,
		f.x, f.y, f.z, 0,
		0, 0, 0, 1
	);
}

//---------------------------------------------------
//-----------------

Quaternion Quaternion::matrix4ToQuaternion(const Matrix4& m)
{
	Vector3 up = Vector3::normalized(Vector3(m.up.x, m.up.y, m.up.z));
	Vector3 forward = Vector3::normalized(Vector3(m.forward.x, m.forward.y, m.forward.z));
	Vector3 right = Vector3::crossProduct(up, forward);
	up = Vector3::crossProduct(forward, right);

	return lookRotation(forward, up);
}

