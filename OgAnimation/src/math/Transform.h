#pragma once

#include "core/CoreMinimal.h"

struct Transform
{
	/*-----------------------------------------------------------------------------
	Attributes and constructors
	-----------------------------------------------------------------------------*/

	/*The position of the transform*/
	Vector3 position;
	/*The rotation of the transform*/
	Quaternion rotation;
	/*The scale of the transform*/
	Vector3 scale;
	
	/*Constructors*/
	explicit inline Transform() : position(Vector3::zeroVector), rotation(Quaternion()), scale(Vector3::unitVector) { }
	explicit inline Transform(const Vector3& p, const Quaternion& r, const Vector3& s) : position(p), rotation(r), scale(s) { }

	/*-----------------------------------------------------------------------------
	Methods
	-----------------------------------------------------------------------------*/

	/**
	 * Equalize this to t transform
	 */
	Transform& operator=(const Transform& t);

	/**
	 * Return the inverse transform of t
	 */
	static Transform inverse(const Transform& t);

	/**
	 * Interpolation between a and b given a t
	 * 
	 * @param t: The time from 0 to 1 -> 0 = a, 1 = b
	 * @return The interpolated transform
	 */
	static Transform mix(const Transform& a, const Transform& b, float t);

	/**
	 * Return the Matrix4 of the t transform
	 */
	static Matrix4 transformToMatrix4(const Transform& t);

	/**
	 * Return the Transform of the m Matrix4
	 */
	static Transform matrix4ToTransform(const Matrix4& m);

	/**
	 * Move a point b in space using given the a transform
	 *
	 * @return The point moved in space
	 */
	static Vector3 transformPoint(const Transform& a, const Vector3& b);

	/**
	 * Move a vector b in space using given the a transform
	 *
	 * @return The vector moved in space
	 */
	static Vector3 transformVector(const Transform& a, const Vector3& b);
};

/*-----------------------------------------------------------------------------
Operators

Operators are NOT member functions to avoid left/right-hand problems
-----------------------------------------------------------------------------*/

Transform operator+(const Transform& a, const Transform& b);
std::ostream& operator<<(std::ostream& os, const Transform& t);
