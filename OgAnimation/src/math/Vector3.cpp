#include "Vector3.h"
#include "OgMath.h"

/*-----------------------------------------------------------------------------
Globals
-----------------------------------------------------------------------------*/

const Vector3 Vector3::zeroVector(0.0f, 0.0f, 0.0f);
const Vector3 Vector3::unitVector(1.0f, 1.0f, 1.0f);
const Vector3 Vector3::upVector(0.0f, 1.0f, 0.0f);
const Vector3 Vector3::downVector(0.0f, -1.0f, 0.0f);
const Vector3 Vector3::forwardVector(0.0f, 0.0f, 1.0f);
const Vector3 Vector3::backwardVector(0.0f, 0.0f, -1.0f);
const Vector3 Vector3::rightVector(1.0f, 0.0f, 0.0f);
const Vector3 Vector3::leftVector(-1.0f, 0.0f, 0.0f);

/*-----------------------------------------------------------------------------
Operators
-----------------------------------------------------------------------------*/

Vector3 operator+(const Vector3& v, const Vector3& w)
{
	return Vector3(v.x + w.x, v.y + w.y, v.z + w.z);
}

//---------------------------------------------------
//-----------------

Vector3 operator-(const Vector3& v, const Vector3& w)
{
	return Vector3(v.x - w.x, v.y - w.y, v.z - w.z);
}

//---------------------------------------------------
//-----------------

Vector3 operator-(const Vector3& v)
{
	return Vector3(-v.x, -v.y, -v.z);
}

//---------------------------------------------------
//-----------------

Vector3 operator*(const Vector3& v, float scale)
{
	return Vector3(v.x * scale, v.y * scale, v.z * scale);
}

//---------------------------------------------------
//-----------------

Vector3 operator*(const Vector3& v, const Vector3& w)
{
	return Vector3(v.x * w.x, v.y * w.y, v.z * w.z);
}

//---------------------------------------------------
//-----------------

Vector3& Vector3::operator=(const Vector3& v)
{
	this->x = v.x;
	this->x = v.y;
	this->x = v.z;

	return *this;
}

//---------------------------------------------------
//-----------------

bool operator==(const Vector3& v, const Vector3& w)
{
	Vector3 diff(v - w);

	return Vector3::lengthSqrt(diff) < EPSILON;
}

//---------------------------------------------------
//-----------------

bool operator!=(const Vector3& v, const Vector3& w)
{
	return !(v == w);
}

//---------------------------------------------------
//-----------------

std::ostream& operator<<(std::ostream& os, const Vector3& v)
{
	os << "X:" << v.x << " |" << "Y:" << v.y << " |" << "Z:" << v.z << "\n";
	return os;
}

/*-----------------------------------------------------------------------------
Methods
-----------------------------------------------------------------------------*/

float Vector3::dotProduct(const Vector3& v, const Vector3& w)
{
	return v.x * w.x + v.y * w.y + v.z * w.z;
}

//---------------------------------------------------
//-----------------

Vector3 Vector3::crossProduct(const Vector3& v, const Vector3& w)
{
	return Vector3(
		v.y * w.z - v.z * w.y,
		v.z * w.x - v.x * w.z,
		v.x * w.y - v.y * w.x
	);
}

//---------------------------------------------------
//-----------------

float Vector3::length()
{
	if (this->lengthSqrt() < EPSILON) { return 0.0f; }

	return sqrtf(this->lengthSqrt());
}

//---------------------------------------------------
//-----------------

float Vector3::length(const Vector3& v)
{
	if (lengthSqrt(v) < EPSILON) { return 0.0f; }

	return sqrtf(lengthSqrt(v));
}

//---------------------------------------------------
//-----------------

float Vector3::lengthSqrt()
{
	return dotProduct(*this, *this);
}

//---------------------------------------------------
//-----------------

float Vector3::lengthSqrt(const Vector3& v)
{
	return dotProduct(v, v);
}

//---------------------------------------------------
//-----------------

float Vector3::distanceTo(const Vector3& v)
{
	return (*this - v).length();
}

//---------------------------------------------------
//-----------------

float Vector3::distanceTo(const Vector3& v, const Vector3& w)
{
	return (v - w).length();
}

//---------------------------------------------------
//-----------------

bool Vector3::normalize()
{
	if (lengthSqrt() < EPSILON) { return false; }

	float invLen = 1.0f / sqrtf(lengthSqrt());

	x *= invLen;
	y *= invLen;
	z *= invLen;

	return true;
}

//---------------------------------------------------
//-----------------

Vector3 Vector3::normalized(const Vector3& v)
{
	if (lengthSqrt(v) < EPSILON) { return v; }

	float invLen = 1.0f / sqrtf(lengthSqrt(v));

	return Vector3
	(
		v.x * invLen,
		v.y * invLen,
		v.z * invLen
	);
}

//---------------------------------------------------
//-----------------

float Vector3::angle(const Vector3& v, const Vector3& w, bool degrees)
{
	float dot = dotProduct(normalized(v), normalized(w));

	if (degrees)
		return acos(dot) * TO_DEGREES;
	return acosf(dot);
}

//---------------------------------------------------
//-----------------

Vector3 Vector3::project(const Vector3& v,const Vector3& w)
{
	float lenghtW = length(w);

	if (lenghtW < EPSILON)
		return Vector3();

	float scale = dotProduct(normalized(v), normalized(w));

	return w * scale;
}

//---------------------------------------------------
//-----------------

Vector3 Vector3::reject(const Vector3& v, const Vector3& w)
{
	return v - project(v, w);
}

//---------------------------------------------------
//-----------------

Vector3 Vector3::reflect(const Vector3& v, const Vector3& w)
{
	float lengthSq = length(w);
	if (lengthSq < EPSILON)
		return Vector3();

	float scale = dotProduct(v, w) / lengthSq;

	Vector3 proj2 = w * (scale * 2);
	return v - proj2;
}

//---------------------------------------------------
//-----------------

Vector3 Vector3::lerp(const Vector3& v, const Vector3& w, float t)
{
	return Vector3(
		v.x + (w.x - v.x) * t,
		v.y + (w.y - v.y) * t,
		v.z + (w.z - v.z) * t
	);
}

//---------------------------------------------------
//-----------------

Vector3 Vector3::slerp(const Vector3& v, const Vector3& w, float t)
{
	if (t < 0.01f)
		return lerp(v, w, t);

	Vector3 from = normalized(v);
	Vector3 to = normalized(w);

	float angleVectors = angle(v, w);

	float a = sinf((1.0f - t) * angleVectors) / sinf(angleVectors);
	float b = sinf(t * angleVectors) / sinf(angleVectors);

	return from * a + to * b;
}

//---------------------------------------------------
//-----------------

Vector3 Vector3::nlerp(const Vector3& v, const Vector3& w, float t)
{
	Vector3 linear(
		v.x + (w.x - v.x) * t,
		v.y + (w.y - v.y) * t,
		v.z + (w.z - v.z) * t
	);

	return normalized(linear);
}





