#include "OgMath.h"

float OgMath::zero(float v)
{
	return v < EPSILON ? 0.0f : v;
}

bool OgMath::isNearlyZero(float v)
{
	return v < EPSILON;
}
