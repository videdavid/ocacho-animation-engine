#define _CRT_SECURE_NO_WARNINGS
#include "Shader.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <glad/glad.h>
#include "GLFW/glfw3.h"

Shader::Shader()
{
	m_handle = glCreateProgram();
}

Shader::Shader(const std::string& vertex, const std::string& fragment)
{
	m_handle = glCreateProgram();
	//load(vertex, fragment);
}

Shader::~Shader()
{
	glDeleteProgram(m_handle);
}

void Shader::load(const std::string& vertex, const std::string& fragment)
{
	std::ifstream file(vertex.c_str());
	bool vertexFile = file.good();
	file.close();

	file = std::ifstream(fragment.c_str());
	bool fragmentFile = file.good();
	file.close();

	std::string vertexSource = vertex;
	if (vertexFile)
		vertexSource = readFile(vertex);

	std::string fragmentSource = fragment;
	if (fragmentFile)
		fragmentSource = readFile(fragment);

	unsigned int vertexHandle = compileVertexShader(vertexSource);
	unsigned int fragmentHandle = compileVertexShader(fragmentSource);

	if(linkShaders(vertexHandle,fragmentHandle))
	{
		fillAttributes();
		fillUniforms();
	}

}

void Shader::bind()
{
	glUseProgram(m_handle);
}

void Shader::unBind()
{
	glUseProgram(0);
}

unsigned int Shader::getAttribute(const std::string& name)
{
	for(auto const& attribute : m_attributes)
		if (attribute.first == name)  return attribute.second; 

	return 0;
}

unsigned int Shader::getUniform(const std::string& name)
{
	for (auto const& uniform : m_uniforms)
		if (uniform.first == name)  return uniform.second; 

	return 0;
}

std::string Shader::readFile(const std::string& path)
{
	std::ifstream file;
	file.open(path);

	std::stringstream contents;
	contents << file.rdbuf();

	file.close();

	return contents.str();
}

unsigned int Shader::compileVertexShader(const std::string& vertex)
{
	unsigned int vertexHandle = glCreateShader(GL_VERTEX_SHADER);
	const char* vertexSource = vertex.c_str();

	glShaderSource(vertexHandle, 1, &vertexSource, NULL);
	glCompileShader(vertexHandle);

	//Check if vertex shader was created successfully
	int success = 0;
	glGetShaderiv(vertexHandle, GL_COMPILE_STATUS, &success);

	if(!success)
	{
		char infoLog[512];
		glGetShaderInfoLog(vertexHandle, 512, NULL, infoLog);
		std::cout << "ERROR: Vertex compilation failed.\n";
		std::cout << "\t" << infoLog << "\n";
		glDeleteShader(vertexHandle);
		return 0;
	}

	return vertexHandle;
}

unsigned int Shader::compileFragmentShader(const std::string& fragment)
{
	unsigned int fragmentHandle = glCreateShader(GL_VERTEX_SHADER);
	const char* fragmentSource = fragment.c_str();

	glShaderSource(fragmentHandle, 1, &fragmentSource, NULL);
	glCompileShader(fragmentHandle);

	//Check if fragment shader was created successfully
	int success = 0;
	glGetShaderiv(fragmentHandle, GL_COMPILE_STATUS, &success);

	if (!success)
	{
		char infoLog[512];
		glGetShaderInfoLog(fragmentHandle, 512, NULL, infoLog);
		std::cout << "ERROR: Fragment compilation failed.\n";
		std::cout << "\t" << infoLog << "\n";
		glDeleteShader(fragmentHandle);
		return 0;
	}

	return fragmentHandle;
}

bool Shader::linkShaders(unsigned int vertex, unsigned int fragment)
{
	glAttachShader(m_handle, vertex);
	glAttachShader(m_handle, fragment);
	glLinkProgram(m_handle);

	int success = 0;
	glGetProgramiv(m_handle, GL_LINK_STATUS, &success);

	if (!success) {
		char infoLog[512];
		glGetProgramInfoLog(m_handle, 512, NULL, infoLog);
		std::cout << "ERROR: Shader linking failed.\n";
		std::cout << "\t" << infoLog << "\n";
		glDeleteShader(vertex);
		glDeleteShader(fragment);
		return false;
	}

	glDeleteShader(vertex);
	glDeleteShader(fragment);

	return true;
}

void Shader::fillAttributes()
{
	int count = -1;
	int length;
	char name[128];
	int size;
	GLenum type;

	bind();

	glGetProgramiv(m_handle, GL_ACTIVE_ATTRIBUTES, &count);

	for (int i=0; i < count; i++)
	{
		memset(name, 0, sizeof(char) * 128);

		glGetActiveAttrib(m_handle, (GLuint)i, 128, &length, &size, &type, name);

		int attribute = glGetAttribLocation(m_handle, name);

		if (attribute >= 0)
			m_attributes[name] = attribute;
	}

	unBind();
}

void Shader::fillUniforms()
{
	int count = -1;
	int length;
	char name[128];
	int size;
	GLenum type;
	char testName[256];

	bind();
	glGetProgramiv(m_handle, GL_ACTIVE_UNIFORMS, &count);

	for (int i = 0; i < count; ++i) {
		memset(name, 0, sizeof(char) * 128);

		glGetActiveUniform(m_handle, (GLuint)i, 128, &length, &size, &type, name);

		int uniform = glGetUniformLocation(m_handle, name);

		if (uniform >= 0) {
			std::string uniformName = name;
			std::size_t found = uniformName.find('[');

			if (found != std::string::npos) {
				uniformName.erase(uniformName.begin() + found, uniformName.end());

				// Populate subscripted names too
				unsigned int uniformIndex = 0;

				while (true) {
					memset(testName, 0, sizeof(char) * 256);
					sprintf(testName, "%s[%d]", uniformName.c_str(), uniformIndex++);
					int uniformLocation = glGetUniformLocation(m_handle, testName);
					if (uniformLocation < 0) {
						break;
					}

					m_uniforms[testName] = uniformLocation;
				}
			}
			m_uniforms[uniformName] = uniform;
		}
	}

	unBind();
}

